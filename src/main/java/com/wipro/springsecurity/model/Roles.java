package com.wipro.springsecurity.model;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "roles")
public class Roles {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	
	public Roles(String name) {
		super();
		this.name = name;
	}
}
